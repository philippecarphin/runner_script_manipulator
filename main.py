
with open('./script.sh', 'r') as f:
    with open('./new_script.sh', 'w+') as ff:
        for s in f.read().splitlines():
            if s == '':
                continue
            if s.strip().startswith('#'):
                continue
            ss = 'echo "\x1b[32;2m$ ' + s + '\x1b[0;m"\n' 
            ff.write(ss)
            ff.write(s + '\n')